package com.chi.service;

import com.chi.pojo.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    public User queryUserByName(String name);

}
